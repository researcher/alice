/* global describe, before, beforeEach, after, afterEach, it, chai, assert */

// eslint-disable-next-line
'use strict';

const Client = require('./../../lib/client').default;

describe('client', function testCase() {
  after(function setup(done) {
    if (Client.connection()) {
      return Client.disconnect()
        .then(done)
        .catch(done);
    }

    return done();
  });

  it('should be able to connect to rabbit mq server using env', function assertion(done) {
    Client.connect()
      .then(function connected(connection) {
        assert.isObject(connection);
        done();
      })
      .catch(done);
  });

  it('should be able to connect to rabbit mq server using plain uri', function assertion(done) {
    Client.connect(process.env.AMQP_URL)
      .then(function connected(connection) {
        assert.isObject(connection);
        done();
      })
      .catch(done);
  });

  it('should be able to disconnect from rabbit mq server', function assertion(done) {
    Client.disconnect()
      .then(function disconnected(connection) {
        assert.isNull(connection);
        done();
      })
      .catch(done);
  });

  it('should be able retain process as background service while consuming',
    function assertion(done) {
      Client.connect()
        .then(function connected() {
          Client.consume({
            exchange: 'integration_test',
            type: 'direct',
            queue: 'test',
            routingKey: 'x.consuming',
          }, function consuming(ctx, next) {
            try {
              assert.equal(JSON.parse(ctx.message.content.toString()), 'testing');
              return next().then(done);
            } catch (err) {
              return done(err);
            }
          });

          setTimeout(function first() {
            Client.publish({
              exchange: 'integration_test',
              type: 'direct',
              routingKey: 'x.consuming',
              content: 'testing',
            });
          }, 100);
        })
        .catch(done);
    })
    .timeout(10000);

  it('should be able retrieve subsequent published message',
    function assertion(done) {
      Client.connect()
        .then(function connected() {
          let subsequentIndex = 0;

          Client.consume({
            exchange: 'integration_test',
            type: 'direct',
            queue: 'ytest',
            routingKey: 'y.consuming',
          }, function consuming(ctx, next) {
            try {
              if (subsequentIndex === 0) {
                assert.equal(JSON.parse(ctx.message.content.toString()), 'testing');
                subsequentIndex++;
                return next();
              }

              if (subsequentIndex === 1) {
                assert.equal(JSON.parse(ctx.message.content.toString()), 'testing2');
                subsequentIndex++;
                return next();
              }

              if (subsequentIndex === 2) {
                assert.equal(JSON.parse(ctx.message.content.toString()), 'testing3');
                return next().then(done);
              }

              return next();
            } catch (err) {
              return done(err);
            }
          });

          setTimeout(function first() {
            Client.publish({
              exchange: 'integration_test',
              type: 'direct',
              routingKey: 'y.consuming',
              content: 'testing',
            });
          }, 100);

          setTimeout(function second() {
            Client.publish({
              exchange: 'integration_test',
              type: 'direct',
              routingKey: 'y.consuming',
              content: 'testing2',
            });
          }, 200);

          setTimeout(function third() {
            Client.publish({
              exchange: 'integration_test',
              type: 'direct',
              routingKey: 'y.consuming',
              content: 'testing3',
            });
          }, 300);
        })
        .catch(done);
    })
    .timeout(10000);
});
