/* global describe, before, beforeEach, after, afterEach, it, chai, assert */

// eslint-disable-next-line
'use strict';

const Client = require('./../../lib/client').default;

describe('client', function testCase() {
  before(function setup(done) {
    if (!Client.connection()) {
      return Client.connect()
        .then(done)
        .catch(done);
    }

    return done();
  });

  after(function setup(done) {
    if (Client.connection()) {
      return Client.disconnect()
        .then(done)
        .catch(done);
    }

    return done();
  });

  it('should', function assertion(done) {
    done();
  });
});
