/* global describe */
/* eslint-disable global-require */

require('dotenv').load();
require('source-map-support').install();

const chai = require('chai');

global.chai = chai;
global.assert = chai.assert;

describe('Integration Test', function testCase() {
  require('./client');
});
