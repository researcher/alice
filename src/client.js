
import _ from 'lodash';
import amqplib from 'amqplib';

let connection = null;
let consumerMiddlewares = [];
let publisherContextExtensions = [];

export default class Client {
  static async connect(url = process.env.AMQP_URL) {
    try {
      const amqpurl = url || 'amqp://localhost';
      connection = await amqplib.connect(amqpurl);

      return connection;
    } catch (err) {
      throw err;
    }
  }

  static connection() {
    return connection;
  }

  static async disconnect() {
    try {
      await connection.close();

      connection = null;

      return connection;
    } catch (err) {
      throw err;
    }
  }

  static use(pkg) {
    this.registerPublisherContextExtension(pkg.publisherExtensions);
    this.registerConsumerMiddleware(pkg.consumerMiddlewares);
  }

  static getConsumerMiddlewares() {
    return consumerMiddlewares;
  }

  static setConsumerMiddlewares(middlewares) {
    consumerMiddlewares = middlewares;

    return consumerMiddlewares;
  }

  static registerConsumerMiddleware(middlewares) {
    if (_.isArray(middlewares)) {
      consumerMiddlewares = consumerMiddlewares.concat(middlewares);

      return consumerMiddlewares;
    }

    consumerMiddlewares.push(middlewares);

    return consumerMiddlewares;
  }

  static getPublisherContextExtensions() {
    return publisherContextExtensions;
  }

  static setPublisherContextExtensions(middlewares) {
    publisherContextExtensions = middlewares;

    return publisherContextExtensions;
  }

  static registerPublisherContextExtension(middlewares) {
    if (_.isArray(middlewares)) {
      publisherContextExtensions = publisherContextExtensions.concat(middlewares);

      return publisherContextExtensions;
    }

    publisherContextExtensions.push(middlewares);

    return publisherContextExtensions;
  }

  static process(handlers) {
    const resultInjector = async (result) => {
      const fns = handlers || [];
      const next = async () => {
        const fn = fns.shift();

        if (!fn) {
          return null;
        }

        return await fn(result, next);
      };

      return await next();
    };

    return resultInjector;
  }

  static async consume({ exchange, type, queue, routingKey }, handler, options) {
    try {
      const channel = await connection.createChannel();

      await channel.assertExchange(exchange, type);
      await channel.assertQueue(queue);
      await channel.bindQueue(queue, exchange, routingKey);

      return await channel.consume(queue, (message) => {
        const context = {};
        context.message = message;
        context.channel = channel;

        Object.assign(context, this);

        channel.ack(message);
        return this.process(consumerMiddlewares.concat(handler))(context);
      }, options);
    } catch (err) {
      throw err;
    }
  }

  static async consumeDirect({ exchange, queue, routingKey }, handler, options) {
    return await this.consume({ exchange, queue, routingKey, type: 'direct' }, handler, options);
  }

  static async consumeFanout({ exchange, queue }, handler, options) {
    return await this.consume({ exchange, queue, type: 'fanout' }, handler, options);
  }

  static async consumeTopic({ exchange, queue, routingKey }, handler, options) {
    return await this.consume({ exchange, queue, routingKey, type: 'topic' }, handler, options);
  }

  // missing exchange durable options
  static async publish({ exchange, type, routingKey, content }, options) {
    try {
      const channel = await connection.createChannel();

      await channel.assertExchange(exchange, type);

      const contentBuffer = new Buffer(JSON.stringify(content));

      const ack = channel.publish(exchange, routingKey, contentBuffer, options);
      const channelClosed = channel.close();
      const context = {
        ack,
        channelClosed,
        meta: {
          exchange,
          routingKey,
          contentBuffer,
          options,
        },
      };

      Object.assign(context, this);

      await this.process(publisherContextExtensions)(context);

      return context;
    } catch (err) {
      throw err;
    }
  }

  static async publishDirect({ exchange, routingKey, content }, options) {
    return await this.publish({ exchange, routingKey, content, type: 'direct' }, options);
  }

  static async publishFanout({ exchange, content }, options) {
    return await this.publish({ exchange, content, type: 'fanout', routingKey: '' }, options);
  }

  static async publishTopic({ exchange, routingKey, content }, options) {
    return await this.publish({ exchange, routingKey, content, type: 'topic' }, options);
  }
}
