'use strict';

exports.__esModule = true;

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _amqplib = require('amqplib');

var _amqplib2 = _interopRequireDefault(_amqplib);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _connection = null;
var consumerMiddlewares = [];
var publisherContextExtensions = [];

var Client = function () {
  function Client() {
    (0, _classCallCheck3.default)(this, Client);
  }

  Client.connect = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee() {
      var url = arguments.length <= 0 || arguments[0] === undefined ? process.env.AMQP_URL : arguments[0];
      var amqpurl;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              amqpurl = url || 'amqp://localhost';
              _context.next = 4;
              return _amqplib2.default.connect(amqpurl);

            case 4:
              _connection = _context.sent;
              return _context.abrupt('return', _connection);

            case 8:
              _context.prev = 8;
              _context.t0 = _context['catch'](0);
              throw _context.t0;

            case 11:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, this, [[0, 8]]);
    }));

    function connect(_x) {
      return _ref.apply(this, arguments);
    }

    return connect;
  }();

  Client.connection = function connection() {
    return _connection;
  };

  Client.disconnect = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2() {
      return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.prev = 0;
              _context2.next = 3;
              return _connection.close();

            case 3:

              _connection = null;

              return _context2.abrupt('return', _connection);

            case 7:
              _context2.prev = 7;
              _context2.t0 = _context2['catch'](0);
              throw _context2.t0;

            case 10:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, this, [[0, 7]]);
    }));

    function disconnect() {
      return _ref2.apply(this, arguments);
    }

    return disconnect;
  }();

  Client.use = function use(pkg) {
    this.registerPublisherContextExtension(pkg.publisherExtensions);
    this.registerConsumerMiddleware(pkg.consumerMiddlewares);
  };

  Client.getConsumerMiddlewares = function getConsumerMiddlewares() {
    return consumerMiddlewares;
  };

  Client.setConsumerMiddlewares = function setConsumerMiddlewares(middlewares) {
    consumerMiddlewares = middlewares;

    return consumerMiddlewares;
  };

  Client.registerConsumerMiddleware = function registerConsumerMiddleware(middlewares) {
    if (_lodash2.default.isArray(middlewares)) {
      consumerMiddlewares = consumerMiddlewares.concat(middlewares);

      return consumerMiddlewares;
    }

    consumerMiddlewares.push(middlewares);

    return consumerMiddlewares;
  };

  Client.getPublisherContextExtensions = function getPublisherContextExtensions() {
    return publisherContextExtensions;
  };

  Client.setPublisherContextExtensions = function setPublisherContextExtensions(middlewares) {
    publisherContextExtensions = middlewares;

    return publisherContextExtensions;
  };

  Client.registerPublisherContextExtension = function registerPublisherContextExtension(middlewares) {
    if (_lodash2.default.isArray(middlewares)) {
      publisherContextExtensions = publisherContextExtensions.concat(middlewares);

      return publisherContextExtensions;
    }

    publisherContextExtensions.push(middlewares);

    return publisherContextExtensions;
  };

  Client.process = function process(handlers) {
    var _this = this;

    var resultInjector = function () {
      var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(result) {
        var fns, next;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                fns = handlers || [];

                next = function () {
                  var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3() {
                    var fn;
                    return _regenerator2.default.wrap(function _callee3$(_context3) {
                      while (1) {
                        switch (_context3.prev = _context3.next) {
                          case 0:
                            fn = fns.shift();

                            if (fn) {
                              _context3.next = 3;
                              break;
                            }

                            return _context3.abrupt('return', null);

                          case 3:
                            _context3.next = 5;
                            return fn(result, next);

                          case 5:
                            return _context3.abrupt('return', _context3.sent);

                          case 6:
                          case 'end':
                            return _context3.stop();
                        }
                      }
                    }, _callee3, _this);
                  }));

                  return function next() {
                    return _ref4.apply(this, arguments);
                  };
                }();

                _context4.next = 4;
                return next();

              case 4:
                return _context4.abrupt('return', _context4.sent);

              case 5:
              case 'end':
                return _context4.stop();
            }
          }
        }, _callee4, _this);
      }));

      return function resultInjector(_x3) {
        return _ref3.apply(this, arguments);
      };
    }();

    return resultInjector;
  };

  Client.consume = function () {
    var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(_ref6, handler, options) {
      var exchange = _ref6.exchange;
      var type = _ref6.type;
      var queue = _ref6.queue;
      var routingKey = _ref6.routingKey;

      var _this2 = this;

      var _ret;

      return _regenerator2.default.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              _context6.prev = 0;
              return _context6.delegateYield(_regenerator2.default.mark(function _callee5() {
                var channel;
                return _regenerator2.default.wrap(function _callee5$(_context5) {
                  while (1) {
                    switch (_context5.prev = _context5.next) {
                      case 0:
                        _context5.next = 2;
                        return _connection.createChannel();

                      case 2:
                        channel = _context5.sent;
                        _context5.next = 5;
                        return channel.assertExchange(exchange, type);

                      case 5:
                        _context5.next = 7;
                        return channel.assertQueue(queue);

                      case 7:
                        _context5.next = 9;
                        return channel.bindQueue(queue, exchange, routingKey);

                      case 9:
                        _context5.next = 11;
                        return channel.consume(queue, function (message) {
                          var context = {};
                          context.message = message;
                          context.channel = channel;

                          Object.assign(context, _this2);

                          channel.ack(message);
                          return _this2.process(consumerMiddlewares.concat(handler))(context);
                        }, options);

                      case 11:
                        _context5.t0 = _context5.sent;
                        return _context5.abrupt('return', {
                          v: _context5.t0
                        });

                      case 13:
                      case 'end':
                        return _context5.stop();
                    }
                  }
                }, _callee5, _this2);
              })(), 't0', 2);

            case 2:
              _ret = _context6.t0;

              if (!((typeof _ret === 'undefined' ? 'undefined' : (0, _typeof3.default)(_ret)) === "object")) {
                _context6.next = 5;
                break;
              }

              return _context6.abrupt('return', _ret.v);

            case 5:
              _context6.next = 10;
              break;

            case 7:
              _context6.prev = 7;
              _context6.t1 = _context6['catch'](0);
              throw _context6.t1;

            case 10:
            case 'end':
              return _context6.stop();
          }
        }
      }, _callee6, this, [[0, 7]]);
    }));

    function consume(_x4, _x5, _x6) {
      return _ref5.apply(this, arguments);
    }

    return consume;
  }();

  Client.consumeDirect = function () {
    var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7(_ref8, handler, options) {
      var exchange = _ref8.exchange;
      var queue = _ref8.queue;
      var routingKey = _ref8.routingKey;
      return _regenerator2.default.wrap(function _callee7$(_context7) {
        while (1) {
          switch (_context7.prev = _context7.next) {
            case 0:
              _context7.next = 2;
              return this.consume({ exchange: exchange, queue: queue, routingKey: routingKey, type: 'direct' }, handler, options);

            case 2:
              return _context7.abrupt('return', _context7.sent);

            case 3:
            case 'end':
              return _context7.stop();
          }
        }
      }, _callee7, this);
    }));

    function consumeDirect(_x7, _x8, _x9) {
      return _ref7.apply(this, arguments);
    }

    return consumeDirect;
  }();

  Client.consumeFanout = function () {
    var _ref9 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8(_ref10, handler, options) {
      var exchange = _ref10.exchange;
      var queue = _ref10.queue;
      return _regenerator2.default.wrap(function _callee8$(_context8) {
        while (1) {
          switch (_context8.prev = _context8.next) {
            case 0:
              _context8.next = 2;
              return this.consume({ exchange: exchange, queue: queue, type: 'fanout' }, handler, options);

            case 2:
              return _context8.abrupt('return', _context8.sent);

            case 3:
            case 'end':
              return _context8.stop();
          }
        }
      }, _callee8, this);
    }));

    function consumeFanout(_x10, _x11, _x12) {
      return _ref9.apply(this, arguments);
    }

    return consumeFanout;
  }();

  Client.consumeTopic = function () {
    var _ref11 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee9(_ref12, handler, options) {
      var exchange = _ref12.exchange;
      var queue = _ref12.queue;
      var routingKey = _ref12.routingKey;
      return _regenerator2.default.wrap(function _callee9$(_context9) {
        while (1) {
          switch (_context9.prev = _context9.next) {
            case 0:
              _context9.next = 2;
              return this.consume({ exchange: exchange, queue: queue, routingKey: routingKey, type: 'topic' }, handler, options);

            case 2:
              return _context9.abrupt('return', _context9.sent);

            case 3:
            case 'end':
              return _context9.stop();
          }
        }
      }, _callee9, this);
    }));

    function consumeTopic(_x13, _x14, _x15) {
      return _ref11.apply(this, arguments);
    }

    return consumeTopic;
  }();

  // missing exchange durable options


  Client.publish = function () {
    var _ref13 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee10(_ref14, options) {
      var exchange = _ref14.exchange;
      var type = _ref14.type;
      var routingKey = _ref14.routingKey;
      var content = _ref14.content;
      var channel, contentBuffer, ack, channelClosed, context;
      return _regenerator2.default.wrap(function _callee10$(_context10) {
        while (1) {
          switch (_context10.prev = _context10.next) {
            case 0:
              _context10.prev = 0;
              _context10.next = 3;
              return _connection.createChannel();

            case 3:
              channel = _context10.sent;
              _context10.next = 6;
              return channel.assertExchange(exchange, type);

            case 6:
              contentBuffer = new Buffer(JSON.stringify(content));
              ack = channel.publish(exchange, routingKey, contentBuffer, options);
              channelClosed = channel.close();
              context = {
                ack: ack,
                channelClosed: channelClosed,
                meta: {
                  exchange: exchange,
                  routingKey: routingKey,
                  contentBuffer: contentBuffer,
                  options: options
                }
              };


              Object.assign(context, this);

              _context10.next = 13;
              return this.process(publisherContextExtensions)(context);

            case 13:
              return _context10.abrupt('return', context);

            case 16:
              _context10.prev = 16;
              _context10.t0 = _context10['catch'](0);
              throw _context10.t0;

            case 19:
            case 'end':
              return _context10.stop();
          }
        }
      }, _callee10, this, [[0, 16]]);
    }));

    function publish(_x16, _x17) {
      return _ref13.apply(this, arguments);
    }

    return publish;
  }();

  Client.publishDirect = function () {
    var _ref15 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee11(_ref16, options) {
      var exchange = _ref16.exchange;
      var routingKey = _ref16.routingKey;
      var content = _ref16.content;
      return _regenerator2.default.wrap(function _callee11$(_context11) {
        while (1) {
          switch (_context11.prev = _context11.next) {
            case 0:
              _context11.next = 2;
              return this.publish({ exchange: exchange, routingKey: routingKey, content: content, type: 'direct' }, options);

            case 2:
              return _context11.abrupt('return', _context11.sent);

            case 3:
            case 'end':
              return _context11.stop();
          }
        }
      }, _callee11, this);
    }));

    function publishDirect(_x18, _x19) {
      return _ref15.apply(this, arguments);
    }

    return publishDirect;
  }();

  Client.publishFanout = function () {
    var _ref17 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee12(_ref18, options) {
      var exchange = _ref18.exchange;
      var content = _ref18.content;
      return _regenerator2.default.wrap(function _callee12$(_context12) {
        while (1) {
          switch (_context12.prev = _context12.next) {
            case 0:
              _context12.next = 2;
              return this.publish({ exchange: exchange, content: content, type: 'fanout', routingKey: '' }, options);

            case 2:
              return _context12.abrupt('return', _context12.sent);

            case 3:
            case 'end':
              return _context12.stop();
          }
        }
      }, _callee12, this);
    }));

    function publishFanout(_x20, _x21) {
      return _ref17.apply(this, arguments);
    }

    return publishFanout;
  }();

  Client.publishTopic = function () {
    var _ref19 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee13(_ref20, options) {
      var exchange = _ref20.exchange;
      var routingKey = _ref20.routingKey;
      var content = _ref20.content;
      return _regenerator2.default.wrap(function _callee13$(_context13) {
        while (1) {
          switch (_context13.prev = _context13.next) {
            case 0:
              _context13.next = 2;
              return this.publish({ exchange: exchange, routingKey: routingKey, content: content, type: 'topic' }, options);

            case 2:
              return _context13.abrupt('return', _context13.sent);

            case 3:
            case 'end':
              return _context13.stop();
          }
        }
      }, _callee13, this);
    }));

    function publishTopic(_x22, _x23) {
      return _ref19.apply(this, arguments);
    }

    return publishTopic;
  }();

  return Client;
}();

exports.default = Client;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9jbGllbnQuanMiXSwibmFtZXMiOlsiY29ubmVjdGlvbiIsImNvbnN1bWVyTWlkZGxld2FyZXMiLCJwdWJsaXNoZXJDb250ZXh0RXh0ZW5zaW9ucyIsIkNsaWVudCIsImNvbm5lY3QiLCJ1cmwiLCJwcm9jZXNzIiwiZW52IiwiQU1RUF9VUkwiLCJhbXFwdXJsIiwiZGlzY29ubmVjdCIsImNsb3NlIiwidXNlIiwicGtnIiwicmVnaXN0ZXJQdWJsaXNoZXJDb250ZXh0RXh0ZW5zaW9uIiwicHVibGlzaGVyRXh0ZW5zaW9ucyIsInJlZ2lzdGVyQ29uc3VtZXJNaWRkbGV3YXJlIiwiZ2V0Q29uc3VtZXJNaWRkbGV3YXJlcyIsInNldENvbnN1bWVyTWlkZGxld2FyZXMiLCJtaWRkbGV3YXJlcyIsImlzQXJyYXkiLCJjb25jYXQiLCJwdXNoIiwiZ2V0UHVibGlzaGVyQ29udGV4dEV4dGVuc2lvbnMiLCJzZXRQdWJsaXNoZXJDb250ZXh0RXh0ZW5zaW9ucyIsImhhbmRsZXJzIiwicmVzdWx0SW5qZWN0b3IiLCJyZXN1bHQiLCJmbnMiLCJuZXh0IiwiZm4iLCJzaGlmdCIsImNvbnN1bWUiLCJoYW5kbGVyIiwib3B0aW9ucyIsImV4Y2hhbmdlIiwidHlwZSIsInF1ZXVlIiwicm91dGluZ0tleSIsImNyZWF0ZUNoYW5uZWwiLCJjaGFubmVsIiwiYXNzZXJ0RXhjaGFuZ2UiLCJhc3NlcnRRdWV1ZSIsImJpbmRRdWV1ZSIsIm1lc3NhZ2UiLCJjb250ZXh0IiwiT2JqZWN0IiwiYXNzaWduIiwiYWNrIiwiY29uc3VtZURpcmVjdCIsImNvbnN1bWVGYW5vdXQiLCJjb25zdW1lVG9waWMiLCJwdWJsaXNoIiwiY29udGVudCIsImNvbnRlbnRCdWZmZXIiLCJCdWZmZXIiLCJKU09OIiwic3RyaW5naWZ5IiwiY2hhbm5lbENsb3NlZCIsIm1ldGEiLCJwdWJsaXNoRGlyZWN0IiwicHVibGlzaEZhbm91dCIsInB1Ymxpc2hUb3BpYyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFFQSxJQUFJQSxjQUFhLElBQWpCO0FBQ0EsSUFBSUMsc0JBQXNCLEVBQTFCO0FBQ0EsSUFBSUMsNkJBQTZCLEVBQWpDOztJQUVxQkMsTTs7Ozs7U0FDTkMsTzs7VUFBUUMsRyx5REFBTUMsUUFBUUMsR0FBUixDQUFZQyxROzs7Ozs7O0FBRTdCQyxxQixHQUFVSixPQUFPLGtCOztxQkFDSixrQkFBUUQsT0FBUixDQUFnQkssT0FBaEIsQzs7O0FBQW5CVCx5QjsrQ0FFT0EsVzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztTQU1KQSxVLHlCQUFhO0FBQ2xCLFdBQU9BLFdBQVA7QUFDRCxHOztTQUVZVSxVOzs7Ozs7OztxQkFFSFYsWUFBV1csS0FBWCxFOzs7O0FBRU5YLDRCQUFhLElBQWI7O2dEQUVPQSxXOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1NBTUpZLEcsZ0JBQUlDLEcsRUFBSztBQUNkLFNBQUtDLGlDQUFMLENBQXVDRCxJQUFJRSxtQkFBM0M7QUFDQSxTQUFLQywwQkFBTCxDQUFnQ0gsSUFBSVosbUJBQXBDO0FBQ0QsRzs7U0FFTWdCLHNCLHFDQUF5QjtBQUM5QixXQUFPaEIsbUJBQVA7QUFDRCxHOztTQUVNaUIsc0IsbUNBQXVCQyxXLEVBQWE7QUFDekNsQiwwQkFBc0JrQixXQUF0Qjs7QUFFQSxXQUFPbEIsbUJBQVA7QUFDRCxHOztTQUVNZSwwQix1Q0FBMkJHLFcsRUFBYTtBQUM3QyxRQUFJLGlCQUFFQyxPQUFGLENBQVVELFdBQVYsQ0FBSixFQUE0QjtBQUMxQmxCLDRCQUFzQkEsb0JBQW9Cb0IsTUFBcEIsQ0FBMkJGLFdBQTNCLENBQXRCOztBQUVBLGFBQU9sQixtQkFBUDtBQUNEOztBQUVEQSx3QkFBb0JxQixJQUFwQixDQUF5QkgsV0FBekI7O0FBRUEsV0FBT2xCLG1CQUFQO0FBQ0QsRzs7U0FFTXNCLDZCLDRDQUFnQztBQUNyQyxXQUFPckIsMEJBQVA7QUFDRCxHOztTQUVNc0IsNkIsMENBQThCTCxXLEVBQWE7QUFDaERqQixpQ0FBNkJpQixXQUE3Qjs7QUFFQSxXQUFPakIsMEJBQVA7QUFDRCxHOztTQUVNWSxpQyw4Q0FBa0NLLFcsRUFBYTtBQUNwRCxRQUFJLGlCQUFFQyxPQUFGLENBQVVELFdBQVYsQ0FBSixFQUE0QjtBQUMxQmpCLG1DQUE2QkEsMkJBQTJCbUIsTUFBM0IsQ0FBa0NGLFdBQWxDLENBQTdCOztBQUVBLGFBQU9qQiwwQkFBUDtBQUNEOztBQUVEQSwrQkFBMkJvQixJQUEzQixDQUFnQ0gsV0FBaEM7O0FBRUEsV0FBT2pCLDBCQUFQO0FBQ0QsRzs7U0FFTUksTyxvQkFBUW1CLFEsRUFBVTtBQUFBOztBQUN2QixRQUFNQztBQUFBLDZFQUFpQixrQkFBT0MsTUFBUDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDZkMsbUJBRGUsR0FDVEgsWUFBWSxFQURIOztBQUVmSSxvQkFGZTtBQUFBLHlGQUVSO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNMQyw4QkFESyxHQUNBRixJQUFJRyxLQUFKLEVBREE7O0FBQUEsZ0NBR05ELEVBSE07QUFBQTtBQUFBO0FBQUE7O0FBQUEsOERBSUYsSUFKRTs7QUFBQTtBQUFBO0FBQUEsbUNBT0VBLEdBQUdILE1BQUgsRUFBV0UsSUFBWCxDQVBGOztBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRlE7O0FBQUEsa0NBRWZBLElBRmU7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQSx1QkFZUkEsTUFaUTs7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQWpCOztBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQU47O0FBZUEsV0FBT0gsY0FBUDtBQUNELEc7O1NBRVlNLE87b0dBQStDQyxPLEVBQVNDLE87VUFBOUNDLFEsU0FBQUEsUTtVQUFVQyxJLFNBQUFBLEk7VUFBTUMsSyxTQUFBQSxLO1VBQU9DLFUsU0FBQUEsVTs7Ozs7Ozs7Ozs7Ozs7Ozs7OytCQUVwQnRDLFlBQVd1QyxhQUFYLEU7OztBQUFoQkMsK0I7OytCQUVBQSxRQUFRQyxjQUFSLENBQXVCTixRQUF2QixFQUFpQ0MsSUFBakMsQzs7OzsrQkFDQUksUUFBUUUsV0FBUixDQUFvQkwsS0FBcEIsQzs7OzsrQkFDQUcsUUFBUUcsU0FBUixDQUFrQk4sS0FBbEIsRUFBeUJGLFFBQXpCLEVBQW1DRyxVQUFuQyxDOzs7OytCQUVPRSxRQUFRUixPQUFSLENBQWdCSyxLQUFoQixFQUF1QixVQUFDTyxPQUFELEVBQWE7QUFDL0MsOEJBQU1DLFVBQVUsRUFBaEI7QUFDQUEsa0NBQVFELE9BQVIsR0FBa0JBLE9BQWxCO0FBQ0FDLGtDQUFRTCxPQUFSLEdBQWtCQSxPQUFsQjs7QUFFQU0saUNBQU9DLE1BQVAsQ0FBY0YsT0FBZDs7QUFFQUwsa0NBQVFRLEdBQVIsQ0FBWUosT0FBWjtBQUNBLGlDQUFPLE9BQUt0QyxPQUFMLENBQWFMLG9CQUFvQm9CLE1BQXBCLENBQTJCWSxPQUEzQixDQUFiLEVBQWtEWSxPQUFsRCxDQUFQO0FBQ0QseUJBVFksRUFTVlgsT0FUVSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztTQWVKZSxhO29HQUErQ2hCLE8sRUFBU0MsTztVQUF4Q0MsUSxTQUFBQSxRO1VBQVVFLEssU0FBQUEsSztVQUFPQyxVLFNBQUFBLFU7Ozs7OztxQkFDL0IsS0FBS04sT0FBTCxDQUFhLEVBQUVHLGtCQUFGLEVBQVlFLFlBQVosRUFBbUJDLHNCQUFuQixFQUErQkYsTUFBTSxRQUFyQyxFQUFiLEVBQThESCxPQUE5RCxFQUF1RUMsT0FBdkUsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7U0FHRmdCLGE7cUdBQW1DakIsTyxFQUFTQyxPO1VBQTVCQyxRLFVBQUFBLFE7VUFBVUUsSyxVQUFBQSxLOzs7Ozs7cUJBQ3hCLEtBQUtMLE9BQUwsQ0FBYSxFQUFFRyxrQkFBRixFQUFZRSxZQUFaLEVBQW1CRCxNQUFNLFFBQXpCLEVBQWIsRUFBa0RILE9BQWxELEVBQTJEQyxPQUEzRCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztTQUdGaUIsWTtzR0FBOENsQixPLEVBQVNDLE87VUFBeENDLFEsVUFBQUEsUTtVQUFVRSxLLFVBQUFBLEs7VUFBT0MsVSxVQUFBQSxVOzs7Ozs7cUJBQzlCLEtBQUtOLE9BQUwsQ0FBYSxFQUFFRyxrQkFBRixFQUFZRSxZQUFaLEVBQW1CQyxzQkFBbkIsRUFBK0JGLE1BQU0sT0FBckMsRUFBYixFQUE2REgsT0FBN0QsRUFBc0VDLE9BQXRFLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBR2Y7OztTQUNha0IsTzt1R0FBaURsQixPO1VBQXZDQyxRLFVBQUFBLFE7VUFBVUMsSSxVQUFBQSxJO1VBQU1FLFUsVUFBQUEsVTtVQUFZZSxPLFVBQUFBLE87Ozs7Ozs7O3FCQUV6QnJELFlBQVd1QyxhQUFYLEU7OztBQUFoQkMscUI7O3FCQUVBQSxRQUFRQyxjQUFSLENBQXVCTixRQUF2QixFQUFpQ0MsSUFBakMsQzs7O0FBRUFrQiwyQixHQUFnQixJQUFJQyxNQUFKLENBQVdDLEtBQUtDLFNBQUwsQ0FBZUosT0FBZixDQUFYLEM7QUFFaEJMLGlCLEdBQU1SLFFBQVFZLE9BQVIsQ0FBZ0JqQixRQUFoQixFQUEwQkcsVUFBMUIsRUFBc0NnQixhQUF0QyxFQUFxRHBCLE9BQXJELEM7QUFDTndCLDJCLEdBQWdCbEIsUUFBUTdCLEtBQVIsRTtBQUNoQmtDLHFCLEdBQVU7QUFDZEcsd0JBRGM7QUFFZFUsNENBRmM7QUFHZEMsc0JBQU07QUFDSnhCLG9DQURJO0FBRUpHLHdDQUZJO0FBR0pnQiw4Q0FISTtBQUlKcEI7QUFKSTtBQUhRLGU7OztBQVdoQlkscUJBQU9DLE1BQVAsQ0FBY0YsT0FBZCxFQUF1QixJQUF2Qjs7O3FCQUVNLEtBQUt2QyxPQUFMLENBQWFKLDBCQUFiLEVBQXlDMkMsT0FBekMsQzs7O2lEQUVDQSxPOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1NBTUVlLGE7dUdBQWlEMUIsTztVQUFqQ0MsUSxVQUFBQSxRO1VBQVVHLFUsVUFBQUEsVTtVQUFZZSxPLFVBQUFBLE87Ozs7OztxQkFDcEMsS0FBS0QsT0FBTCxDQUFhLEVBQUVqQixrQkFBRixFQUFZRyxzQkFBWixFQUF3QmUsZ0JBQXhCLEVBQWlDakIsTUFBTSxRQUF2QyxFQUFiLEVBQWdFRixPQUFoRSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztTQUdGMkIsYTt1R0FBcUMzQixPO1VBQXJCQyxRLFVBQUFBLFE7VUFBVWtCLE8sVUFBQUEsTzs7Ozs7O3FCQUN4QixLQUFLRCxPQUFMLENBQWEsRUFBRWpCLGtCQUFGLEVBQVlrQixnQkFBWixFQUFxQmpCLE1BQU0sUUFBM0IsRUFBcUNFLFlBQVksRUFBakQsRUFBYixFQUFvRUosT0FBcEUsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7U0FHRjRCLFk7dUdBQWdENUIsTztVQUFqQ0MsUSxVQUFBQSxRO1VBQVVHLFUsVUFBQUEsVTtVQUFZZSxPLFVBQUFBLE87Ozs7OztxQkFDbkMsS0FBS0QsT0FBTCxDQUFhLEVBQUVqQixrQkFBRixFQUFZRyxzQkFBWixFQUF3QmUsZ0JBQXhCLEVBQWlDakIsTUFBTSxPQUF2QyxFQUFiLEVBQStERixPQUEvRCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztrQkE1S0kvQixNIiwiZmlsZSI6ImNsaWVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCBhbXFwbGliIGZyb20gJ2FtcXBsaWInO1xuXG5sZXQgY29ubmVjdGlvbiA9IG51bGw7XG5sZXQgY29uc3VtZXJNaWRkbGV3YXJlcyA9IFtdO1xubGV0IHB1Ymxpc2hlckNvbnRleHRFeHRlbnNpb25zID0gW107XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENsaWVudCB7XG4gIHN0YXRpYyBhc3luYyBjb25uZWN0KHVybCA9IHByb2Nlc3MuZW52LkFNUVBfVVJMKSB7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IGFtcXB1cmwgPSB1cmwgfHwgJ2FtcXA6Ly9sb2NhbGhvc3QnO1xuICAgICAgY29ubmVjdGlvbiA9IGF3YWl0IGFtcXBsaWIuY29ubmVjdChhbXFwdXJsKTtcblxuICAgICAgcmV0dXJuIGNvbm5lY3Rpb247XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICB0aHJvdyBlcnI7XG4gICAgfVxuICB9XG5cbiAgc3RhdGljIGNvbm5lY3Rpb24oKSB7XG4gICAgcmV0dXJuIGNvbm5lY3Rpb247XG4gIH1cblxuICBzdGF0aWMgYXN5bmMgZGlzY29ubmVjdCgpIHtcbiAgICB0cnkge1xuICAgICAgYXdhaXQgY29ubmVjdGlvbi5jbG9zZSgpO1xuXG4gICAgICBjb25uZWN0aW9uID0gbnVsbDtcblxuICAgICAgcmV0dXJuIGNvbm5lY3Rpb247XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICB0aHJvdyBlcnI7XG4gICAgfVxuICB9XG5cbiAgc3RhdGljIHVzZShwa2cpIHtcbiAgICB0aGlzLnJlZ2lzdGVyUHVibGlzaGVyQ29udGV4dEV4dGVuc2lvbihwa2cucHVibGlzaGVyRXh0ZW5zaW9ucyk7XG4gICAgdGhpcy5yZWdpc3RlckNvbnN1bWVyTWlkZGxld2FyZShwa2cuY29uc3VtZXJNaWRkbGV3YXJlcyk7XG4gIH1cblxuICBzdGF0aWMgZ2V0Q29uc3VtZXJNaWRkbGV3YXJlcygpIHtcbiAgICByZXR1cm4gY29uc3VtZXJNaWRkbGV3YXJlcztcbiAgfVxuXG4gIHN0YXRpYyBzZXRDb25zdW1lck1pZGRsZXdhcmVzKG1pZGRsZXdhcmVzKSB7XG4gICAgY29uc3VtZXJNaWRkbGV3YXJlcyA9IG1pZGRsZXdhcmVzO1xuXG4gICAgcmV0dXJuIGNvbnN1bWVyTWlkZGxld2FyZXM7XG4gIH1cblxuICBzdGF0aWMgcmVnaXN0ZXJDb25zdW1lck1pZGRsZXdhcmUobWlkZGxld2FyZXMpIHtcbiAgICBpZiAoXy5pc0FycmF5KG1pZGRsZXdhcmVzKSkge1xuICAgICAgY29uc3VtZXJNaWRkbGV3YXJlcyA9IGNvbnN1bWVyTWlkZGxld2FyZXMuY29uY2F0KG1pZGRsZXdhcmVzKTtcblxuICAgICAgcmV0dXJuIGNvbnN1bWVyTWlkZGxld2FyZXM7XG4gICAgfVxuXG4gICAgY29uc3VtZXJNaWRkbGV3YXJlcy5wdXNoKG1pZGRsZXdhcmVzKTtcblxuICAgIHJldHVybiBjb25zdW1lck1pZGRsZXdhcmVzO1xuICB9XG5cbiAgc3RhdGljIGdldFB1Ymxpc2hlckNvbnRleHRFeHRlbnNpb25zKCkge1xuICAgIHJldHVybiBwdWJsaXNoZXJDb250ZXh0RXh0ZW5zaW9ucztcbiAgfVxuXG4gIHN0YXRpYyBzZXRQdWJsaXNoZXJDb250ZXh0RXh0ZW5zaW9ucyhtaWRkbGV3YXJlcykge1xuICAgIHB1Ymxpc2hlckNvbnRleHRFeHRlbnNpb25zID0gbWlkZGxld2FyZXM7XG5cbiAgICByZXR1cm4gcHVibGlzaGVyQ29udGV4dEV4dGVuc2lvbnM7XG4gIH1cblxuICBzdGF0aWMgcmVnaXN0ZXJQdWJsaXNoZXJDb250ZXh0RXh0ZW5zaW9uKG1pZGRsZXdhcmVzKSB7XG4gICAgaWYgKF8uaXNBcnJheShtaWRkbGV3YXJlcykpIHtcbiAgICAgIHB1Ymxpc2hlckNvbnRleHRFeHRlbnNpb25zID0gcHVibGlzaGVyQ29udGV4dEV4dGVuc2lvbnMuY29uY2F0KG1pZGRsZXdhcmVzKTtcblxuICAgICAgcmV0dXJuIHB1Ymxpc2hlckNvbnRleHRFeHRlbnNpb25zO1xuICAgIH1cblxuICAgIHB1Ymxpc2hlckNvbnRleHRFeHRlbnNpb25zLnB1c2gobWlkZGxld2FyZXMpO1xuXG4gICAgcmV0dXJuIHB1Ymxpc2hlckNvbnRleHRFeHRlbnNpb25zO1xuICB9XG5cbiAgc3RhdGljIHByb2Nlc3MoaGFuZGxlcnMpIHtcbiAgICBjb25zdCByZXN1bHRJbmplY3RvciA9IGFzeW5jIChyZXN1bHQpID0+IHtcbiAgICAgIGNvbnN0IGZucyA9IGhhbmRsZXJzIHx8IFtdO1xuICAgICAgY29uc3QgbmV4dCA9IGFzeW5jICgpID0+IHtcbiAgICAgICAgY29uc3QgZm4gPSBmbnMuc2hpZnQoKTtcblxuICAgICAgICBpZiAoIWZuKSB7XG4gICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gYXdhaXQgZm4ocmVzdWx0LCBuZXh0KTtcbiAgICAgIH07XG5cbiAgICAgIHJldHVybiBhd2FpdCBuZXh0KCk7XG4gICAgfTtcblxuICAgIHJldHVybiByZXN1bHRJbmplY3RvcjtcbiAgfVxuXG4gIHN0YXRpYyBhc3luYyBjb25zdW1lKHsgZXhjaGFuZ2UsIHR5cGUsIHF1ZXVlLCByb3V0aW5nS2V5IH0sIGhhbmRsZXIsIG9wdGlvbnMpIHtcbiAgICB0cnkge1xuICAgICAgY29uc3QgY2hhbm5lbCA9IGF3YWl0IGNvbm5lY3Rpb24uY3JlYXRlQ2hhbm5lbCgpO1xuXG4gICAgICBhd2FpdCBjaGFubmVsLmFzc2VydEV4Y2hhbmdlKGV4Y2hhbmdlLCB0eXBlKTtcbiAgICAgIGF3YWl0IGNoYW5uZWwuYXNzZXJ0UXVldWUocXVldWUpO1xuICAgICAgYXdhaXQgY2hhbm5lbC5iaW5kUXVldWUocXVldWUsIGV4Y2hhbmdlLCByb3V0aW5nS2V5KTtcblxuICAgICAgcmV0dXJuIGF3YWl0IGNoYW5uZWwuY29uc3VtZShxdWV1ZSwgKG1lc3NhZ2UpID0+IHtcbiAgICAgICAgY29uc3QgY29udGV4dCA9IHt9O1xuICAgICAgICBjb250ZXh0Lm1lc3NhZ2UgPSBtZXNzYWdlO1xuICAgICAgICBjb250ZXh0LmNoYW5uZWwgPSBjaGFubmVsO1xuXG4gICAgICAgIE9iamVjdC5hc3NpZ24oY29udGV4dCwgdGhpcyk7XG5cbiAgICAgICAgY2hhbm5lbC5hY2sobWVzc2FnZSk7XG4gICAgICAgIHJldHVybiB0aGlzLnByb2Nlc3MoY29uc3VtZXJNaWRkbGV3YXJlcy5jb25jYXQoaGFuZGxlcikpKGNvbnRleHQpO1xuICAgICAgfSwgb3B0aW9ucyk7XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICB0aHJvdyBlcnI7XG4gICAgfVxuICB9XG5cbiAgc3RhdGljIGFzeW5jIGNvbnN1bWVEaXJlY3QoeyBleGNoYW5nZSwgcXVldWUsIHJvdXRpbmdLZXkgfSwgaGFuZGxlciwgb3B0aW9ucykge1xuICAgIHJldHVybiBhd2FpdCB0aGlzLmNvbnN1bWUoeyBleGNoYW5nZSwgcXVldWUsIHJvdXRpbmdLZXksIHR5cGU6ICdkaXJlY3QnIH0sIGhhbmRsZXIsIG9wdGlvbnMpO1xuICB9XG5cbiAgc3RhdGljIGFzeW5jIGNvbnN1bWVGYW5vdXQoeyBleGNoYW5nZSwgcXVldWUgfSwgaGFuZGxlciwgb3B0aW9ucykge1xuICAgIHJldHVybiBhd2FpdCB0aGlzLmNvbnN1bWUoeyBleGNoYW5nZSwgcXVldWUsIHR5cGU6ICdmYW5vdXQnIH0sIGhhbmRsZXIsIG9wdGlvbnMpO1xuICB9XG5cbiAgc3RhdGljIGFzeW5jIGNvbnN1bWVUb3BpYyh7IGV4Y2hhbmdlLCBxdWV1ZSwgcm91dGluZ0tleSB9LCBoYW5kbGVyLCBvcHRpb25zKSB7XG4gICAgcmV0dXJuIGF3YWl0IHRoaXMuY29uc3VtZSh7IGV4Y2hhbmdlLCBxdWV1ZSwgcm91dGluZ0tleSwgdHlwZTogJ3RvcGljJyB9LCBoYW5kbGVyLCBvcHRpb25zKTtcbiAgfVxuXG4gIC8vIG1pc3NpbmcgZXhjaGFuZ2UgZHVyYWJsZSBvcHRpb25zXG4gIHN0YXRpYyBhc3luYyBwdWJsaXNoKHsgZXhjaGFuZ2UsIHR5cGUsIHJvdXRpbmdLZXksIGNvbnRlbnQgfSwgb3B0aW9ucykge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCBjaGFubmVsID0gYXdhaXQgY29ubmVjdGlvbi5jcmVhdGVDaGFubmVsKCk7XG5cbiAgICAgIGF3YWl0IGNoYW5uZWwuYXNzZXJ0RXhjaGFuZ2UoZXhjaGFuZ2UsIHR5cGUpO1xuXG4gICAgICBjb25zdCBjb250ZW50QnVmZmVyID0gbmV3IEJ1ZmZlcihKU09OLnN0cmluZ2lmeShjb250ZW50KSk7XG5cbiAgICAgIGNvbnN0IGFjayA9IGNoYW5uZWwucHVibGlzaChleGNoYW5nZSwgcm91dGluZ0tleSwgY29udGVudEJ1ZmZlciwgb3B0aW9ucyk7XG4gICAgICBjb25zdCBjaGFubmVsQ2xvc2VkID0gY2hhbm5lbC5jbG9zZSgpO1xuICAgICAgY29uc3QgY29udGV4dCA9IHtcbiAgICAgICAgYWNrLFxuICAgICAgICBjaGFubmVsQ2xvc2VkLFxuICAgICAgICBtZXRhOiB7XG4gICAgICAgICAgZXhjaGFuZ2UsXG4gICAgICAgICAgcm91dGluZ0tleSxcbiAgICAgICAgICBjb250ZW50QnVmZmVyLFxuICAgICAgICAgIG9wdGlvbnMsXG4gICAgICAgIH0sXG4gICAgICB9O1xuXG4gICAgICBPYmplY3QuYXNzaWduKGNvbnRleHQsIHRoaXMpO1xuXG4gICAgICBhd2FpdCB0aGlzLnByb2Nlc3MocHVibGlzaGVyQ29udGV4dEV4dGVuc2lvbnMpKGNvbnRleHQpO1xuXG4gICAgICByZXR1cm4gY29udGV4dDtcbiAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgIHRocm93IGVycjtcbiAgICB9XG4gIH1cblxuICBzdGF0aWMgYXN5bmMgcHVibGlzaERpcmVjdCh7IGV4Y2hhbmdlLCByb3V0aW5nS2V5LCBjb250ZW50IH0sIG9wdGlvbnMpIHtcbiAgICByZXR1cm4gYXdhaXQgdGhpcy5wdWJsaXNoKHsgZXhjaGFuZ2UsIHJvdXRpbmdLZXksIGNvbnRlbnQsIHR5cGU6ICdkaXJlY3QnIH0sIG9wdGlvbnMpO1xuICB9XG5cbiAgc3RhdGljIGFzeW5jIHB1Ymxpc2hGYW5vdXQoeyBleGNoYW5nZSwgY29udGVudCB9LCBvcHRpb25zKSB7XG4gICAgcmV0dXJuIGF3YWl0IHRoaXMucHVibGlzaCh7IGV4Y2hhbmdlLCBjb250ZW50LCB0eXBlOiAnZmFub3V0Jywgcm91dGluZ0tleTogJycgfSwgb3B0aW9ucyk7XG4gIH1cblxuICBzdGF0aWMgYXN5bmMgcHVibGlzaFRvcGljKHsgZXhjaGFuZ2UsIHJvdXRpbmdLZXksIGNvbnRlbnQgfSwgb3B0aW9ucykge1xuICAgIHJldHVybiBhd2FpdCB0aGlzLnB1Ymxpc2goeyBleGNoYW5nZSwgcm91dGluZ0tleSwgY29udGVudCwgdHlwZTogJ3RvcGljJyB9LCBvcHRpb25zKTtcbiAgfVxufVxuIl19