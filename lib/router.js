'use strict';

exports.__esModule = true;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _client = require('./client');

var _client2 = _interopRequireDefault(_client);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Router = function () {
  function Router(baseRoutingKey) {
    (0, _classCallCheck3.default)(this, Router);

    this.baseRoutingKey = baseRoutingKey;
    this.middlewares = [];
  }

  Router.prototype.use = function use(middlewares) {
    if (_lodash2.default.isArray(middlewares)) {
      this.middlewares = this.middlewares.concat(middlewares);

      return this.middlewares;
    }

    this.middlewares.push(middlewares);

    return this.middlewares;
  };

  Router.prototype.transformPath = function transformPath(route) {
    var routePath = ('' + this.baseRoutingKey + route).replace(/^\//, '');

    return routePath.replace(/\//g, '.');
  };

  Router.prototype.direct = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(_ref2, handlers) {
      var exchange = _ref2.exchange;
      var queue = _ref2.queue;
      var route = _ref2.route;
      var options = _ref2.options;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _client2.default.consumeDirect({
                exchange: exchange,
                queue: queue,
                routingKey: this.transformPath(route)
              }, this.middlewares.concat(handlers), options);

            case 2:
              return _context.abrupt('return', _context.sent);

            case 3:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function direct(_x, _x2) {
      return _ref.apply(this, arguments);
    }

    return direct;
  }();

  Router.prototype.fanout = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(_ref4, handlers) {
      var exchange = _ref4.exchange;
      var queue = _ref4.queue;
      var options = _ref4.options;
      return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return _client2.default.consumeFanout({
                exchange: exchange,
                queue: queue
              }, this.middlewares.concat(handlers), options);

            case 2:
              return _context2.abrupt('return', _context2.sent);

            case 3:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));

    function fanout(_x3, _x4) {
      return _ref3.apply(this, arguments);
    }

    return fanout;
  }();

  Router.prototype.topic = function () {
    var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(_ref6, handlers) {
      var exchange = _ref6.exchange;
      var queue = _ref6.queue;
      var route = _ref6.route;
      var options = _ref6.options;
      return _regenerator2.default.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return _client2.default.consumeTopic({
                exchange: exchange,
                queue: queue,
                routingKey: this.transformPath(route)
              }, this.middlewares.concat(handlers), options);

            case 2:
              return _context3.abrupt('return', _context3.sent);

            case 3:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, this);
    }));

    function topic(_x5, _x6) {
      return _ref5.apply(this, arguments);
    }

    return topic;
  }();

  return Router;
}();

exports.default = Router;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9yb3V0ZXIuanMiXSwibmFtZXMiOlsiUm91dGVyIiwiYmFzZVJvdXRpbmdLZXkiLCJtaWRkbGV3YXJlcyIsInVzZSIsImlzQXJyYXkiLCJjb25jYXQiLCJwdXNoIiwidHJhbnNmb3JtUGF0aCIsInJvdXRlIiwicm91dGVQYXRoIiwicmVwbGFjZSIsImRpcmVjdCIsImhhbmRsZXJzIiwiZXhjaGFuZ2UiLCJxdWV1ZSIsIm9wdGlvbnMiLCJjb25zdW1lRGlyZWN0Iiwicm91dGluZ0tleSIsImZhbm91dCIsImNvbnN1bWVGYW5vdXQiLCJ0b3BpYyIsImNvbnN1bWVUb3BpYyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQUNBOzs7O0FBQ0E7Ozs7OztJQUVxQkEsTTtBQUNuQixrQkFBWUMsY0FBWixFQUE0QjtBQUFBOztBQUMxQixTQUFLQSxjQUFMLEdBQXNCQSxjQUF0QjtBQUNBLFNBQUtDLFdBQUwsR0FBbUIsRUFBbkI7QUFDRDs7bUJBRURDLEcsZ0JBQUlELFcsRUFBYTtBQUNmLFFBQUksaUJBQUVFLE9BQUYsQ0FBVUYsV0FBVixDQUFKLEVBQTRCO0FBQzFCLFdBQUtBLFdBQUwsR0FBbUIsS0FBS0EsV0FBTCxDQUFpQkcsTUFBakIsQ0FBd0JILFdBQXhCLENBQW5COztBQUVBLGFBQU8sS0FBS0EsV0FBWjtBQUNEOztBQUVELFNBQUtBLFdBQUwsQ0FBaUJJLElBQWpCLENBQXNCSixXQUF0Qjs7QUFFQSxXQUFPLEtBQUtBLFdBQVo7QUFDRCxHOzttQkFFREssYSwwQkFBY0MsSyxFQUFPO0FBQ25CLFFBQU1DLFlBQVksTUFBRyxLQUFLUixjQUFSLEdBQXlCTyxLQUF6QixFQUFpQ0UsT0FBakMsQ0FBeUMsS0FBekMsRUFBZ0QsRUFBaEQsQ0FBbEI7O0FBRUEsV0FBT0QsVUFBVUMsT0FBVixDQUFrQixLQUFsQixFQUF5QixHQUF6QixDQUFQO0FBQ0QsRzs7bUJBRUtDLE07a0dBQTRDQyxRO1VBQW5DQyxRLFNBQUFBLFE7VUFBVUMsSyxTQUFBQSxLO1VBQU9OLEssU0FBQUEsSztVQUFPTyxPLFNBQUFBLE87Ozs7OztxQkFDeEIsaUJBQU9DLGFBQVAsQ0FBcUI7QUFDaENILGtDQURnQztBQUVoQ0MsNEJBRmdDO0FBR2hDRyw0QkFBWSxLQUFLVixhQUFMLENBQW1CQyxLQUFuQjtBQUhvQixlQUFyQixFQUlWLEtBQUtOLFdBQUwsQ0FBaUJHLE1BQWpCLENBQXdCTyxRQUF4QixDQUpVLEVBSXlCRyxPQUp6QixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OzttQkFPVEcsTTtvR0FBcUNOLFE7VUFBNUJDLFEsU0FBQUEsUTtVQUFVQyxLLFNBQUFBLEs7VUFBT0MsTyxTQUFBQSxPOzs7Ozs7cUJBQ2pCLGlCQUFPSSxhQUFQLENBQXFCO0FBQ2hDTixrQ0FEZ0M7QUFFaENDO0FBRmdDLGVBQXJCLEVBR1YsS0FBS1osV0FBTCxDQUFpQkcsTUFBakIsQ0FBd0JPLFFBQXhCLENBSFUsRUFHeUJHLE9BSHpCLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O21CQU1USyxLO29HQUEyQ1IsUTtVQUFuQ0MsUSxTQUFBQSxRO1VBQVVDLEssU0FBQUEsSztVQUFPTixLLFNBQUFBLEs7VUFBT08sTyxTQUFBQSxPOzs7Ozs7cUJBQ3ZCLGlCQUFPTSxZQUFQLENBQW9CO0FBQy9CUixrQ0FEK0I7QUFFL0JDLDRCQUYrQjtBQUcvQkcsNEJBQVksS0FBS1YsYUFBTCxDQUFtQkMsS0FBbkI7QUFIbUIsZUFBcEIsRUFJVixLQUFLTixXQUFMLENBQWlCRyxNQUFqQixDQUF3Qk8sUUFBeEIsQ0FKVSxFQUl5QkcsT0FKekIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7a0JBeENJZixNIiwiZmlsZSI6InJvdXRlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCBDbGllbnQgZnJvbSAnLi9jbGllbnQnO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSb3V0ZXIge1xuICBjb25zdHJ1Y3RvcihiYXNlUm91dGluZ0tleSkge1xuICAgIHRoaXMuYmFzZVJvdXRpbmdLZXkgPSBiYXNlUm91dGluZ0tleTtcbiAgICB0aGlzLm1pZGRsZXdhcmVzID0gW107XG4gIH1cblxuICB1c2UobWlkZGxld2FyZXMpIHtcbiAgICBpZiAoXy5pc0FycmF5KG1pZGRsZXdhcmVzKSkge1xuICAgICAgdGhpcy5taWRkbGV3YXJlcyA9IHRoaXMubWlkZGxld2FyZXMuY29uY2F0KG1pZGRsZXdhcmVzKTtcblxuICAgICAgcmV0dXJuIHRoaXMubWlkZGxld2FyZXM7XG4gICAgfVxuXG4gICAgdGhpcy5taWRkbGV3YXJlcy5wdXNoKG1pZGRsZXdhcmVzKTtcblxuICAgIHJldHVybiB0aGlzLm1pZGRsZXdhcmVzO1xuICB9XG5cbiAgdHJhbnNmb3JtUGF0aChyb3V0ZSkge1xuICAgIGNvbnN0IHJvdXRlUGF0aCA9IGAke3RoaXMuYmFzZVJvdXRpbmdLZXl9JHtyb3V0ZX1gLnJlcGxhY2UoL15cXC8vLCAnJyk7XG5cbiAgICByZXR1cm4gcm91dGVQYXRoLnJlcGxhY2UoL1xcLy9nLCAnLicpO1xuICB9XG5cbiAgYXN5bmMgZGlyZWN0KHsgZXhjaGFuZ2UsIHF1ZXVlLCByb3V0ZSwgb3B0aW9ucyB9LCBoYW5kbGVycykge1xuICAgIHJldHVybiBhd2FpdCBDbGllbnQuY29uc3VtZURpcmVjdCh7XG4gICAgICBleGNoYW5nZSxcbiAgICAgIHF1ZXVlLFxuICAgICAgcm91dGluZ0tleTogdGhpcy50cmFuc2Zvcm1QYXRoKHJvdXRlKSxcbiAgICB9LCB0aGlzLm1pZGRsZXdhcmVzLmNvbmNhdChoYW5kbGVycyksIG9wdGlvbnMpO1xuICB9XG5cbiAgYXN5bmMgZmFub3V0KHsgZXhjaGFuZ2UsIHF1ZXVlLCBvcHRpb25zIH0sIGhhbmRsZXJzKSB7XG4gICAgcmV0dXJuIGF3YWl0IENsaWVudC5jb25zdW1lRmFub3V0KHtcbiAgICAgIGV4Y2hhbmdlLFxuICAgICAgcXVldWUsXG4gICAgfSwgdGhpcy5taWRkbGV3YXJlcy5jb25jYXQoaGFuZGxlcnMpLCBvcHRpb25zKTtcbiAgfVxuXG4gIGFzeW5jIHRvcGljKHsgZXhjaGFuZ2UsIHF1ZXVlLCByb3V0ZSwgb3B0aW9ucyB9LCBoYW5kbGVycykge1xuICAgIHJldHVybiBhd2FpdCBDbGllbnQuY29uc3VtZVRvcGljKHtcbiAgICAgIGV4Y2hhbmdlLFxuICAgICAgcXVldWUsXG4gICAgICByb3V0aW5nS2V5OiB0aGlzLnRyYW5zZm9ybVBhdGgocm91dGUpLFxuICAgIH0sIHRoaXMubWlkZGxld2FyZXMuY29uY2F0KGhhbmRsZXJzKSwgb3B0aW9ucyk7XG4gIH1cbn1cbiJdfQ==